import "package:signalr_core/signalr_core.dart";

void main() async {
  final connection = HubConnectionBuilder()
      .withUrl(
          'http://hwsrv-672798.hostwindsdns.com:8005',
          HttpConnectionOptions(
            transport: HttpTransportType.webSockets,
            logging: (level, message) => print(message),
            accessTokenFactory: getToken
          ))
      .withAutomaticReconnect()
      .build();

  await connection.start();

  connection.on('Notification', (message) {
    print(message.toString());
  });

  connection.onreconnecting((e) {
    print('Reconnecting yo');
  });
}

Future<String> getToken() async {
    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIwMTkxNjE3MTc2MCIsImp0aSI6ImRhOTEwNjE3LTUyNzktNGRhOS04NDI0LTk5OWM0NzYyZjQyNiIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWVpZGVudGlmaWVyIjoiMTQ0ZDFjMWEtYTJlZS00NTU0LTg4OTMtZjRlNTBmNGM2ODE5IiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIkRldmVsb3BlciIsIkFkbWluIiwiU2VydmljZVByb3ZpZGVyIiwiQ29uc3VtZXIiXSwiZXhwIjoxNTg0MzE0MzkwLCJpc3MiOiJodHRwOi8veW91cmRvbWFpbi5jb20iLCJhdWQiOiJodHRwOi8veW91cmRvbWFpbi5jb20ifQ.xg1Lw05aAErwt4qcQ0qLLHuZ0rAdBriNMaMPsQiKMqE";
  }